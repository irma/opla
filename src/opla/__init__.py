"""A professional webpage generator with a focus on research activities."""

__version__ = "1.1.0"

THEMES = ("water", "materialize", "rawhtml")
