"""Execute OPLA as a module"""

import sys
from .opla import main

if __name__ == "__main__":
    sys.exit(main())
