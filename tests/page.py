from pathlib import Path

from faker import Faker


def create_page(page_path: Path) -> str:
    """
    Generate a markdown file with random content

    Parameters
    ----------
    page_path : Path
        Path to the markdown file to be created

    Returns
    -------
    markdown : str
        The markdown content
    """
    fake = Faker(["fr_FR"])

    config = {
        "title": fake.text(20)[:-1],
        "name": fake.first_name(),
        "occupation": fake.job(),
    }
    content1 = """
### Articles
{{% publications_hal idhal=viktoria-heu doctype=ART%}}
### Chapitres d'ouvrages
{{% publications_hal idhal=viktoria-heu doctype=COUV%}}
### Autres
{{% publications_hal idhal=viktoria-heu doctype=UNDEFINED%}}
### Bibtex
{{% publications_bibtex bibtex=data/bibtex.bib%}}
    """
    pub_section = {"title": "Publications", "content": content1}
    sections = []
    sections.append(pub_section)
    for _ in range(1, fake.random_int(min=3, max=6)):
        title = fake.text(20)[:-1]
        content2 = "\n".join(fake.paragraphs(25))
        section = {"title": title, "content": content2}

        sections.append(section)

    markdown = f"""---
title: {config['title']}
name: Jean Le Rond d'Alembert
theme:
    name: materialize
data:
    - data/img
---"""
    for section in sections:
        markdown += f"\n## {section['title']}\n{section['content']}\n"

    with open(page_path, "w") as f:
        f.write(markdown)

    return markdown


def main():
    create_page("PAGE.md")
