---
title: Jean Le Rond d'Alembert's webpage
name: Jean Le Rond d'Alembert
theme:
    name: materialize
    color: teal
    custom: custom_theme
footer:
    social:
        - src: https://portrait_dAlembert.jpg
          alt: Jean Le Rond d'Alembert
          url: https://en.wikipedia.org/wiki/Jean_le_Rond_d%27Alembert
        - src: img/300.png
          alt: Fake logo
          url: https://www.example.com
---

# A single section

Nothing
