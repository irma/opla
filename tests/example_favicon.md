---
title: Jean Le Rond d'Alembert's webpage
name: Jean Le Rond d'Alembert
theme:
  favicon:
    - rel: icon
      href: img/favicon-96x96.png
      type: image/png
      sizes: 96x96
    - rel: apple-touch-icon
      href: img/apple-touch-icon.png
---

# A single section

Nothing
