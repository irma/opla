---
title: Jean Le Rond d'Alembert's webpage
name: Jean Le Rond d'Alembert
theme:
    name: materialize
    color: teal
footer:
    social:
      - icon: github
        url: https://www.github.com/jlrda
        color: white
      - icon: researchgate
        url: https://www.researchgate.com/jlrda
        color: black
      - icon: twitter
        url: https://www.twitter.com/jlrda
      - icon: gitlab
        url: https://www.gitlab.inria.fr/jlrda
        text: Inria
---

# A single section

Nothing
