# OPLA: One Page Layout Automator

[![pipeline status](https://gitlab.math.unistra.fr/irma/opla/badges/main/pipeline.svg)](https://gitlab.math.unistra.fr/irma/opla/-/commits/main)
[![coverage report](https://gitlab.math.unistra.fr/irma/opla/badges/main/coverage.svg)](https://irma.pages.math.unistra.fr/opla/coverage)
[![Latest Release](https://gitlab.math.unistra.fr/irma/opla/-/badges/release.svg)](https://gitlab.math.unistra.fr/irma/opla/-/releases)
[![pypi package](https://badge.fury.io/py/opla-ssg.svg)](https://pypi.org/project/opla-ssg)
[![Doc](https://img.shields.io/badge/doc-sphinx-blue)](https://irma.pages.math.unistra.fr/opla)

A professional webpage generator with a focus on research activities.

## Features

- Generate a professional webpage from a single markdown file
- Handle publication lists coming from HAL database or from a bibtex file
- Highly customizable thanks to the use of jinja templates, shortcodes and custom styles and scripts

## Example

Here is a preview of a generated page:

![Screenshot of a webpage for Dalembert](dalembert.png)

See complete examples with various graphical themes in this [doc section](https://irma.pages.math.unistra.fr/opla/usage.html#select-your-graphic-theme).
