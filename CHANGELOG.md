# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2025-01-10

### Fixed

- Bug on the example dropdown button in the materialize theme (#59)

### Changed

- The example icon is now Bluesky instead of Twitter (#62)
- Use the *real* bibliography of D'Alembert in the documentation (#72)
- Better code coverage using mocking

### Added

- the HAL bibliography shortcode now accepts the ORCID (#54)

## [1.0.0] - 2024-12-23

### Fixed

- A `build/` directory is always created when running `opla` (#63)

### Removed

- No longer use an explicit list of css and js files in the theme configuration (#69)

### Added

- Make it possible to use a custom templates directory (#66)
- Use logging module for logging (#67)
- Style h1 in the materialize theme
- Make it possible to execute opla as a module
- Handle favicon in the theme configuration (#64)

### Changed

- Enhance code for handling payload
- Custom style must come after the theme style
- `markdown` module is renamed `parse`
- custom CSS and JS files must be in the `my_theme/static/css/` and `my_theme/static/js` directories respectively (#69)

## [0.3.1] - 2024-10-31

### Changed

- Drop the `sgelt` dependency and use the [`mdslicer`](https://github.com/boileaum/mdslicer) package instead (see #61)

### Fixed

- Sphinx cross-references warning

## [0.3.0] - 2024-09-27

### Added

- A smooth scroll in the materialize theme
- Doc: add a dropdown button to choose the color of the materialize theme example (see #59)
- Render LaTeX equations (see #56)

### Changed

- Reorganize the templates to make them more modular and easier to maintain
- Use a `ChoiceLoader` for jinja (see #42)
- Doc: examples menu is now a grid of image cards (see #60)
- `opla.md` is the default file name for CLI (see #55)

## [0.2.2] - 2024-09-27

### Added

- Add setuptools to dependency list

## [0.2.1] - 2024-06-14

### Added

- Update the documentation with Myst-Markdown link
- Add documentation for making the examples
- opla version in footer (#53)

### Changed

- Use tag message as release description
- Reorg the templates (#50 and #52)

### Fixed

- Hover effects in footer icons

## [0.2.0] - 2024-06-11

### Added

- Handle footer data in markdown header
- A number of examples in the documentation

### Changed

- Reorganize code into modules
- custom css and js files may apply to all themes

### Removed

- useless test files

### Fixed

- A number of issues with the theme

## [0.1.1] - 2024-06-06

### Changed

- Use PYPI for `sgelt` dependency in order to publish opla on PYPI.
- Change project name to be able to publish on PYPI.

## [0.1.0] - 2024-06-05

### Added

- First release.
