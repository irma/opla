# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from opla import __version__

project = "opla"
copyright = "2024 IRMA"
author = "IRMA"
release = __version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

exclude_patterns = ["_autosummary/opla.rst"]

extensions = [
    "myst_parser",
    "sphinx_design",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
    "sphinx.ext.autosummary",
]

templates_path = ["_templates"]


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"

autodoc_default_options = {
    "members": True,  # Include all members (methods, attributes)
    "show-inheritance": True,  # Show inherited members
    "member-order": "groupwise",  # Group members
    "private-members": False,  # Do not include private members
    "special-members": "__init__",  # Include __init__ method
}
autodoc_typehints = "description"  # Include type hints in the description

autosummary_generate = True  # Turn on sphinx.ext.autosummary
add_module_names = False  # Remove module names from class/method signatures
myst_heading_anchors = 3  # Handle anchor links

# Furo theme options
html_theme_options = {
    "source_edit_link": "https://gitlab.math.unistra.fr/irma/opla/-/edit/main/docs/source/{filename}?ref_type=heads",
}

# Myst extension options
myst_enable_extensions = {
    "attrs_inline": True,
    "colon_fence": True,
}
