```{include} ../../README.md
```

## Installation

```bash
pip install opla-ssg
```

## Documentation

```{toctree}
:maxdepth: 2

usage.md
developers_corner.md
```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
