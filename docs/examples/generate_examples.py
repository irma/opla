"""A Python script to generate the examples from a template file."""

import jinja2

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader("."),
    block_start_string="<%",
    block_end_string="%>",
    variable_start_string="<<",
    variable_end_string=">>",
)
template = env.get_template("example.j2.md")

# Tuple of example dicts
examples = (
    {"name": "rawhtml", "logo_color": "black"},
    {"name": "water", "logo_color": "black"},
    {"name": "materialize", "color": "teal", "logo_color": "white"},
)
# Tuple of theme names
theme_names = tuple(example["name"] for example in examples)

for example in examples:
    result = template.render(
        **example, themes=theme_names, theme_selector=True
    )
    with open(f"{example['name']}.md", "w") as f:
        f.write(result)

materialize_example = template.render(
    name="materialize", color="teal", logo_color="white", themes=theme_names
)
with open("materialize_example.md", "w") as f:
    f.write(materialize_example)
