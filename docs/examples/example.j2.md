---
title: Jean Le Rond d'Alembert's webpage
name: Jean Le Rond d'Alembert
theme:
    name: << name >>
    <%- if color %>
    color: << color >>
    <%- endif %>
    <%- if theme_selector %>
    custom: theme
    <%- endif %>
data:
    - img
    - cv.pdf
footer:
    contact:
      - "<jlrda@dix-huitieme-siecle.fr>"
      - "Six feet under the carrefour de Chateaudun-Place Kossuth, 75009 Paris, France"
      - "+33 1 01 02 03 04"
    social:
      - icon: github
        url: https://www.github.com/jlrda
        color: << logo_color >>
      - icon: researchgate
        url: https://www.researchgate.com/jlrda
        color: << logo_color >>
      - icon: bluesky
        url: https://bsky.app/profile/jlrda.bsky.social
        color: << logo_color >>
      - icon: gitlab
        url: https://www.gitlab.inria.fr/jlrda
        color: << logo_color >>
        text: Inria
      - icon: gitlab
        url: https://www.gitlab.com/jlrda
        color: << logo_color >>
        text: .com
---

<%- if theme_selector %>
<form action="#" method="post" id="choose-theme">
    <span class="<< color >>-text"><b>Choose theme:</b></span>

    <% for theme in themes %>
    <label>
    <input name="theme" class="with-gap" id="<< theme >>" type="radio" << "checked" if name == theme >> />
    <span><code><< theme >></code></span>
    </label>
    <% endfor %>

</form>
<%- endif %>

![D'Alembert portrait](img/portrait.jpg){: .responsive-img .z-depth-3 .left .mr-6 width="185px"}

I am a French mathematician, physicist, philosopher, and encyclopedist.

I am famous for having been the inventor of a principle of equilibrium that Condorcet explains in his Éloge de d'Alembert.
I thus established a connection between the laws of motion.
Through my theorem, now known as the *d'Alembert's theorem,* I perceive the presence of *n* roots in any algebraic equation of degree *n*.
I invented this new branch of mathematics, the calculus of partial derivatives, which introduces arbitrary functions.
Following my research in mathematics on differential equations and partial derivatives, I was called upon to lead the Encyclopédie with Denis Diderot.
Schools, streets, and research centers bear my name.

I established a formula named after me for the solution of the wave equation:

$$
\frac{\partial^2 u}{\partial t^2} - c^2 \frac{\partial^2 u}{\partial x^2} = 0
$$

You can download my curriculum vitae [here](cv.pdf).

## Biography

I was born on November 16, 1717, in Paris, France.
I was abandoned by my parents at the age of two and raised by the wife of a glazier.
I was educated at the Collège des Quatre-Nations, where I studied law and mathematics.
I later became a prominent figure in the intellectual circles of Paris, where I associated with the leading thinkers of the Enlightenment, including Voltaire, Diderot, and Rousseau.

I am best known for my work in mathematics and mechanics, particularly for my contributions to the study of partial differential equations and the principle of virtual work.
I also made significant contributions to the fields of fluid dynamics, celestial mechanics, and the philosophy of science.

I passed away on October 29, 1783, at the Vieux Louvre.
The priest of Saint-Germain l'Auxerrois refused for me to be buried in the church with an "inscription worthy of my fame".
On October 31, 1783, my body was accompanied by a long procession to the Porcherons cemetery where I was buried.
My eulogy was delivered by Nicolas de Condorcet.

## Research Interests

My research interests span a wide range of topics in mathematics and physics, including:

- Partial differential equations
- Fluid dynamics
- Celestial mechanics
- Philosophy of science
- History of mathematics
- Music theory
- Social sciences
- Literature
- Fine arts
- ...


## Publications

Uncompleted list taken from <http://dalembert.academie-sciences.fr/bibliographie.php>.

### Books

{{% publications_bibtex bibtex=dalembert.bib type="book" %}}

### Articles

{{% publications_bibtex bibtex=dalembert.bib type="article" %}}


## Awards and Honors

- Académie royale des sciences de Prusse (1746)
- Royal Society (1748)
- Académie des sciences (1754)
- Académie française (1754-1783)
- Académie des sciences de Saint-Pétersbourg (1773)
- Académie américaine des arts et des sciences (1781)
- Académie royale suédoise des belles-lettres, d'histoire et des antiquités
- Société royale des lettres et des sciences de Norvège
- Académie des sciences de Turin

## Seminars and Talks

- *On the stability of fluid flows*, Séminaire de mathématiques appliquées, Université Paris-Sud, 20XX
- *The principle of virtual work in mechanics*, Séminaire de mécanique, École Normale Supérieure, 20XX
- *The philosophy of science in the Enlightenment*, Séminaire de philosophie, Université Paris-Sorbonne, 20XX
- *The history of mathematics in France*, Séminaire d'histoire des sciences, Collège de France, 20XX

## Image Gallery

<div class="carousel mb-6">
    <a class="carousel-item" href="#!"><img src="img/encyclopedie.jpg" class="materialboxed" width="200px" data-caption="Encyclopedie de D'Alembert et Diderot - Premiere Page"></a>
    <a class="carousel-item" href="#!"><img src="img/nouvelles_experiences.jpg" class="materialboxed" width="200px" data-caption="Nouvelles expériences sur la résistance des fluides"></a>
    <a class="carousel-item" href="#!"><img src="img/chambre_obscure.png" class="materialboxed" width="200px" data-caption="Camera obscura in Encyclopédie, ou Dictionnaire raisonné des Sciences, des Arts et des Métiers, 1763."></a>
</div>
