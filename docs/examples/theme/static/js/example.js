// Add href to the theme radio buttons
window.onload = function () {
  var radios = document.getElementsByName('theme');

  for (var i = 0; i < radios.length; i++) {
    radios[i].onchange = function () {
      /* Get the theme name by its input id */
      currentTheme = this.id;
      // Store current theme in a global variable
      window.location.href = "../" + currentTheme + "/index.html";
    }
  }
}


//List of all colors in Materialize
const colors = [
  'red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown', 'blue-grey', 'grey'
];

//Variable to store the current color
let currentColor = 'teal';
function replaceMaterializeColor(oldColor, newColor) {
  //Select all elements on the page except those from the dropdown content
  const elements = document.querySelectorAll(':not(.dropdown-content *)');
  -
    //Cycle through all elements
    elements.forEach(element => {
      //Check if the element has a class
      if (element.classList.length > 0) {
        //Convert list of classes to array
        const classList = Array.from(element.classList);

        //Cycle through all classes of the element
        classList.forEach(className => {
          //If the class contains the old color, replace it with the new color
          if (className.includes(oldColor)) {
            const newClassName = className.replace(oldColor, newColor);
            element.classList.replace(className, newClassName);
          }
        });
      }
    });

  //Update current color
  currentColor = newColor;
}

// Create a function that returns the current theme
function getCurrentTheme() {
  return window.location.href.split("/").at(-2);
}

document.addEventListener('DOMContentLoaded', function () {
  const currentTheme = getCurrentTheme();
  // Create a dropdown button only if current theme is materialize
  if (currentTheme !== 'materialize') {
    return;
  }

  //Create a drop-down menu with all the colors after the "Choose theme" form
  const dropdown = document.createElement('div');
  dropdown.classList.add('dropdown');
  dropdown.innerHTML = `
      <a class="dropdown-trigger btn ${currentColor}" href="#" data-target="dropdown1">Choose a color</a>
      <ul id="dropdown1" class="dropdown-content">
${colors.map(color => `<li><a href="#!" onclick="replaceMaterializeColor(currentColor, '${color}')" class="${color}-text" style="color: inherit;">${color}</a></li>`).join('')}
      </ul>
    `;
  console.log(dropdown.innerHTML);
  const chooseThemeElement = document.getElementById('choose-theme');
  if (chooseThemeElement) {
    chooseThemeElement.insertAdjacentElement('afterend', dropdown);
  } else {
    console.error('Element with class "choose-theme" not found.');
  }

});

document.addEventListener('DOMContentLoaded', function () {
  const currentTheme = getCurrentTheme();
  // Create a dropdown button only if current theme is materialize
  if (currentTheme !== 'materialize') {
    return;
  }
  var elems = document.querySelectorAll('.dropdown-trigger');
  var instances = M.Dropdown.init(elems);
});
