"""
Get HAL doctype codes description from HAL API and write them to a CSV file
"""

from typing import Dict

import requests  # type: ignore


def get_doctype_description(doctypes: list) -> Dict[str, str]:
    """
    Get the doctype description from the HAL API

    Parameters:
        doctypes: list of doctype dictionaries

    Returns:
        doctypes_description: dictionary of {code: description}
    """

    doctypes_description = {
        doctype["str"][0]: doctype["str"][1] for doctype in doctypes
    }
    # sort the dictionary by key
    doctypes_description = dict(sorted(doctypes_description.items()))
    return doctypes_description


hal_url = "https://api.archives-ouvertes.fr/ref/doctype"

# Get HAL data
response = requests.get(hal_url)
json_data = response.json()

docs = json_data["response"]["result"]["doc"]

doctypes_description = get_doctype_description(docs)

csv_path = "source/hal_doctypes.csv"

with open(csv_path, "w") as f:
    for key, value in doctypes_description.items():
        f.write(f"{key};{value}\n")
    print(f"HAL doctypes written to {csv_path}")
